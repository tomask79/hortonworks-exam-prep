case class Item(a: Int, b: Int) {
   def sum: Int = {
        return this.a + this.b;
   }
}

def funct(a: Int, b: Int): Int = {
  return a + b;
}

def printFunct(a: Int, b: Int): Unit = {
  println("a :"+a+" b:"+b);
}

val seq = Array(new Item(1, 2), new Item(3, 4), new Item(4, 4));

var rdd = sc.parallelize(seq.toSeq);

rdd.foreach(item=>printFunct(item.a, item.b));

rdd.collect();
