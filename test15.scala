case class worker(id: Int, name:String, sex:String, country:String);

val fileRDD = sc.textFile("/tests/workers.txt");

val objectsRDD = fileRDD.map(line=>worker(line.split(",")(0).toInt, line.split(",")(1), line.split(",")(2), line.split(",")(3)));

val workersDF = objectsRDD.toDF();

workersDF.registerTempTable("workers");

val statsDF = sqlContext.sql("select sex,country,count(*) as pocet from workers where sex='female' group by sex,country");

statsDF.show();
