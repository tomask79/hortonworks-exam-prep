import org.apache.spark.sql.hive._;

case class worker(id: Int, name: String, sex: String, country: String);

val hc = new HiveContext(sc);

hc.sql("USE xademo");

val fileRDD = sc.textFile("/tests/workers.txt");

val dfWorkers = fileRDD.map(line=>new worker(line.split(",")(0).toInt, line.split(",")(1), line.split(",")(2), line.split(",")(3))).toDF();

// saved as ORC
dfWorkers.write.mode("overwrite").orc("orcWorkers1");

// read it as ORC
val dfReadORC = hc.read.orc("orcWorkers1");

dfReadORC.show();
