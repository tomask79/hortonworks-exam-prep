case class eorders(id: Int, amount: Int);

val fileRDD = sc.textFile("/tests/eorders.txt");

val objectsRDD = fileRDD.map(line=>eorders(line.split(",")(0).toInt,line.split(",")(1).toInt));

val eordersDF = objectsRDD.toDF();

eordersDF.registerTempTable("eorders");

val sumDF = eordersDF.groupBy("id").agg(sum("amount").as("sum_amount"),avg("amount").as("avg_amount")).registerTempTable("esummary");

val sumLowerAvg = sqlContext.sql("SELECT s.id, s.amount FROM eorders s INNER JOIN esummary e ON s.id = e.id AND s.amount < e.avg_amount"); 

sumLowerAvg.show(); 
