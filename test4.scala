case class Flight (
	employee: String,
	date: String,
	city: String,
	counter: Integer
)

var rows = sc.textFile("/tests/flights.txt");
var objectsRows = rows.map(row=>Flight(row.split(" ")(0), row.split(" ")(1), row.split(" ")(2), 1));
var newYorkOnly = objectsRows.filter(flight=>flight.city.equals("NewYork"));
var newYorkPairs = newYorkOnly.map(newYork=>(newYork.employee, newYork));
var newYorkCounter = newYorkPairs.reduceByKey((a,b)=>Flight(a.employee, a.date, a.city, a.counter+b.counter));
newYorkCounter.collect();

//var sortedNewYork = newYorkCounter.sortBy(flight=>flight._2, false);
//sortedNewYork.take(1);

System.exit(0); 
