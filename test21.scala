val seqrdd = sc.parallelize(Seq((1,2), (13,1), (14,10)));

val seqrdd2 = sc.parallelize(Seq((1,2), (1,11), (1,56)));

//val seqrdd3 = sc.parallelize(Seq((1, "hh"), (6, "h"), (8, "k")));

//val result = seqrdd.leftOuterJoin(seqrdd2);

//val interRDD = seqrdd.intersection(seqrdd2);

val groupedKey = seqrdd.union(seqrdd2).groupByKey();

groupedKey.mapValues(value=>value.size).map(tuple=>(tuple._2, tuple._1)).sortByKey(false).take(1).foreach(println);

//val fullOuterRDD = seqrdd.fullOuterJoin(seqrdd2);

//fullOuterRDD.collect();

