case class student(id: Int, name: String);

val studentsFileRDD = sc.textFile("/tests/students.txt");

val studentsDF = studentsFileRDD.map(line=>student(line.split(",")(0).toInt, line.split(",")(1))).toDF();

studentsDF.registerTempTable("students");

val df = sqlContext.sql("SELECT id,name FROM students WHERE id=1");

df.show();

val backRDD = df.rdd;

val mappedRDD = backRDD.map(row=>row(0)+","+row(1)).saveAsTextFile("/tests/ID1Student.txt");

System.exit(0);
