case class Employee (
	id: String,
	name: String,
	role: String
);

case class Travel (
	employee_id: String,
	city: String,
	state: String
);

case class Result (
	employee_id: String,
	name: String,
	role: String,
	city: String
);

val fileEmployees = sc.textFile("/tests/employees.txt");
val fileTravels = sc.textFile("/tests/e_travels.txt");

val employeesObjects = fileEmployees.map(line=>Employee(line.split(",")(0), line.split(",")(1), line.split(",")(2)));
val travelsObjects = fileTravels.map(line=>Travel(line.split(",")(0), line.split(",")(1), line.split(",")(2)));

val ePairs = employeesObjects.map(employee=>(employee.id, employee));
val tPairs = travelsObjects.map(travel=>(travel.employee_id, travel)).filter(pair=>pair._2.state.equals("USA"));

val rPairs = tPairs.map(travel=>(travel._1, 1));

val traverel = rPairs.reduceByKey((a,b)=>a+b).map(pair=>(pair._2, pair._1)).sortByKey(false).map(pair=>(pair._2, pair._1)).take(1);

val joined = ePairs.join(tPairs.filter(travel=>travel._1.equals(traverel(0)._1)));

joined.collect();

//val result = joined.map(row=>(Result(row._1, row._2._1.name, row._2._1.role, row._2._2.city)));

//result.map(result=>result.name+","+result.city).saveAsTextFile("/tests/result1.txt");

System.exit(0);
