import org.apache.spark.sql.hive._;

val hc = new HiveContext(sc);

hc.sql("USE xademo");

//hc.sql("SHOW TABLES").show();

val anotherDF = hc.sql("SELECT * from myTable WHERE id = 1");

anotherDF.write.mode("overwrite").saveAsTable("xademo.justONE");

hc.sql("SELECT * FROM justONE").show();
