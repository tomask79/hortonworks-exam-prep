val rddFile = sc.textFile("/tests/words.txt");
val rddWords = rddFile.flatMap(line=>line.split(" "));

val accumulator = sc.accumulator(0.0);

rddWords.foreach(word=>if (word.equals("Spark")) {
	accumulator.add(1);
});

System.out.println(accumulator.value);
