var file = sc.textFile("/tests/data.txt");
var words = file.flatMap(row=>row.split(" "));
var filtered = words.distinct();
var pairs = filtered.map(word=>(word,1));
val counts = pairs.reduceByKey((a,b)=>a+b);
counts.collect();
System.exit(0);
