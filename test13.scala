case class order(id: Int, amount: Int);

val fileRDD = sc.textFile("/tests/eorders.txt");

val ordersRDD = fileRDD.map(line=>order(line.split(",")(0).toInt, line.split(",")(1).toInt));

val accumCount = sc.accumulator(0);

val sumAccum = sc.accumulator(0.0);

ordersRDD.foreach(orderItem=>{
accumCount.add(1);
sumAccum.add(orderItem.amount);
});

System.out.println(sumAccum.value/accumCount.value);

val avgNum = sumAccum.value / accumCount.value;

val avgBroadcast = sc.broadcast(avgNum);

val filteredRDD = ordersRDD.filter(item=>item.amount < avgBroadcast.value);

filteredRDD.map(item=>item.id+","+item.amount).saveAsTextFile("/tests/broadcast1.txt");
