import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

case class Student(id: Int,
		   name: String);

val students = sc.textFile("/tests/students.txt");
val attendence = sc.textFile("/tests/attendence.txt");

val studentsObjects = students.map(line=>(line.split(",")(0).toInt, Student(line.split(",")(0).toInt,line.split(",")(1))));

val attendencePairs = attendence.flatMap(line=>{
	for(i <- 1 to line.split(",").length-1) yield {
		(line.split(",")(0).toInt, line.split(",")(i));
	}
}
);

val filterDate = LocalDate.of(2018,1,13);

val studentPairs = attendencePairs.filter(at=>{
	         val localDate = LocalDate.parse(at._2, DateTimeFormatter.ofPattern("MM.dd.yyyy"));
		 localDate.isAfter(filterDate);
});

val countPairs = studentPairs.map(st=>(st._1,1)).reduceByKey((a,b)=>a+b).map(st=>(st._2,st._1)).sortByKey(false).map(st=>(st._2, st._1));

val studentResult = countPairs.take(1)(0);

val joinResult = studentPairs.join(studentsObjects.filter(st=>st._2.id.equals(studentResult._1)));

val printResult = joinResult.map(at=>(at._2._2.name, at._2._1));

printResult.collect();
System.exit(0);
