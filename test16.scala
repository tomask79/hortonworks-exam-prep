import org.apache.spark.sql.hive._;

case class worker(id: Int, name: String, sex: String, country: String);

val hc = new HiveContext(sc);

hc.sql("USE xademo");

hc.sql("create table IF NOT EXISTS workersXA (id int, name String, sex String, country String)");

val workersRDD = sc.textFile("/tests/workers.txt");

val objectsDF = workersRDD.map(line=>new worker(
			line.split(",")(0).toInt, 
			line.split(",")(1), 
			line.split(",")(2), 
			line.split(",")(3))
).toDF();

objectsDF.write.mode("overwrite").saveAsTable("xademo.myTableXADemo");

hc.sql("SHOW TABLES").show();

hc.sql("SELECT * FROM myTableXADemo").show();
