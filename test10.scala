case class employee(id: Int, name: String, surname: String);

case class travels(e_id: Int, city: String, country: String);

val empRDD = sc.textFile("/tests/employees.txt");

val empObjects = empRDD.map(line=>employee(line.split(",")(0).toInt,line.split(",")(1),line.split(",")(2)));

val empDF = empObjects.toDF();

val etravelRDD = sc.textFile("/tests/e_travels.txt");

val etravelObjects = etravelRDD.map(line=>travels(line.split(",")(0).toInt,line.split(",")(1), line.split(",")(2)));

val etravelDF = etravelObjects.toDF();

empDF.registerTempTable("employee");
etravelDF.registerTempTable("travels");

val maxTravel = etravelDF.filter("country = 'USA'").groupBy("e_id").count().sort(desc("count")).first(); // Row

val resultFrame = sqlContext.sql("SELECT e.name, e.surname, t.city FROM employee e INNER JOIN travels t ON e.id = t.e_id AND t.e_id = "+maxTravel(0));

resultFrame.show();

System.exit(0);
