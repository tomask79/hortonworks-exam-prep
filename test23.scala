import org.apache.spark.sql.hive._;
import org.apache.spark.storage.StorageLevel;

case class movie(id: Int, name: String, country: String, genre: String);

val fileRDD = sc.textFile("/tests/movies.txt");
val hc = new HiveContext(sc);

hc.sql("use xademo");

val movieDF = fileRDD.map(line=>new movie(line.split(",")(0).toInt, line.split(",")(1), line.split(",")(2), line.split(",")(3))).toDF();

movieDF.registerTempTable("movie");

val rddGrouped = sqlContext.sql("select genre, count(*) as pocet from movie group by genre order by pocet desc").rdd;

val genrePair = rddGrouped.take(1);

val resultDF = sqlContext.sql("select * from movie where genre = '"+genrePair(0)(0)+"'");

resultDF.write.mode("overwrite").saveAsTable("xademo.genres");

hc.sql("SHOW TABLES").show();

//hc.sql("select * from genres").show();