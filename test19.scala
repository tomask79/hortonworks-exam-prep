import org.apache.spark.sql.hive._;

val hc = new HiveContext(sc);

hc.sql("create table IF NOT EXISTS persons(name String, city String, age int) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\,' LINES TERMINATED BY '\\n' ");

hc.sql("load data inpath '/tests/persons.txt' into table persons");

hc.sql("select * from persons").show();
