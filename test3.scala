case class Flight(
	employee: String,
	date: String,
	city: String
)

var rows = sc.textFile("/tests/flights.txt");
var wordsByClass = rows.map(line=>Flight(line.split(" ")(0), line.split(" ")(1), line.split(" ")(2)));
var employeePairs = wordsByClass.map(flight=>(flight.employee, 1));
var flightsCounting = employeePairs.reduceByKey((a,b)=>a+b);
var sortedFlights = flightsCounting.sortBy(flightItem=>flightItem._2, false);
sortedFlights.take(1);
System.exit(0);
