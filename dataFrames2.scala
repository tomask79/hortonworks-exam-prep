import org.apache.spark.sql.types._;

import org.apache.spark.sql._;

case class SchemaItem(getName:String, getType:String);

val fileRDD = sc.textFile("/spark/persons.txt");

val schemaItemList = sc.textFile("/spark/personsMetadata.txt").
				flatMap(line=>line.split(",")).map(
					schemaItem=>new SchemaItem(schemaItem.split("-")(0), schemaItem.split("-")(1))
			).collect().toList;


var schema = new StructType();

schemaItemList.foreach (schemaRow => {
    schema = schema.add(schemaRow.getName, schemaRow.getType);
});

val rowRDD = fileRDD.map(line=>line.split(",")).map(lineArray=>Row(lineArray(0), lineArray(1), lineArray(2).toInt));

val df = sqlContext.createDataFrame(rowRDD, schema);

df.show();

df.printSchema();

