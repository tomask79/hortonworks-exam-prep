var rows = sc.textFile("/tests/data.txt");
var words = rows.flatMap(row=>row.split(" "));
var pairs = words.map(word=>(word,1));
var occurences = pairs.reduceByKey((a,b)=>a+b);
var result = occurences.filter(pair => pair._2 > 1);
result.collect();
System.exit(0);
