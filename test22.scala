case class Movie(id:Int,name:String,country:String,genre:String);

val fileMoviesRDD = sc.textFile("/tests/movies.txt");
val objectsMovies = fileMoviesRDD.map(line=>new Movie(line.split(",")(0).toInt,
                                                     line.split(",")(1),
                                                     line.split(",")(2),
                                                     line.split(",")(3)));
// Task: Which country produces horror's most often
val horrorsRDD = objectsMovies.filter(movie=>movie.genre.equals("horror")).
                        map(hmovie=>(hmovie.country,1));

// val horrorCountry = sc.parallelize(horrorsRDD.countByKey().toSeq).map(hmovie=>(hmovie._2,hmovie._1)).sortByKey(false).take(1);
// countByKey = key, numberOfOccurence...Map..toSeq.

//val horrorCountry = horrorsRDD.reduceByKey((a,b)=>a+b).map(hmovie=>(hmovie._2,hmovie._1)).sortByKey(false).take(1);

val horrorCountryValues = horrorsRDD.countByValue();
// countByValue = (key,value),numberOfOccurrence ...toSeq.

//println("Country with most horrors: "+horrorCountry(0)._2);

System.exit(0);