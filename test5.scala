case class Flight(
	employee: String,
	date: String,
	city: String);

var rows = sc.textFile("/tests/flights.txt");
var objects = rows.map(line=>Flight(line.split(" ")(0), line.split(" ")(1), line.split(" ")(2)));
var pairs = objects.map(flight=>(flight.employee, 1));

var groups = pairs.groupByKey();
groups.collect();

System.exit(0);
